import {buildApiFile} from "../src/doc/doc";
import importApi from "../src/doc";


it('doc api test', () => {
    buildApiFile({
      methods: 'home3'.split(',')
    })
});

it('doc api import', () => {
  importApi({
    methods: 'home3'.split(',')
  }, true, {})
});

it('doc api test8', () => {
  importApi({
    methods: 'home'.split(',')
  }, true, {})
});
