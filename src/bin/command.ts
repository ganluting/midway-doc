#!/usr/bin/env node
import {Command} from "commander"
import {buildApiFile, config} from "../doc/doc";
import importApi from "../doc";

const program = new Command();

program.version('v' + require('../../package.json').version)
  .description('当前版本号')
  .option('-p ,--path <path>', '解析一个目录,默认为当前目录下./src/controller')
  .option('-m ,--method <method>', '解析多个method逗号分隔')
  .option('-c ,--controller <controller>', '解析多个controller逗号分隔')
  .option('-o ,--output <output>', '输出目录');

program.command('yapi')
  .description('创建本地yapi配置文件,用于自动提交文档')
  .action((args: any) => {
    const fs = require('fs');
    const paths = require('path');
    const filePath = paths.resolve('./', './yapi.json');
    const config = {
      enable: true,
      serviceUrl: '',
      token: '',
    };
    if (fs.existsSync(filePath)) {
      console.log('配置文件已存在');
      return;
    } else {
      fs.writeFileSync(filePath, JSON.stringify(config, null, 2), 'utf-8');
      console.log('配置文件创建成功, 路径:'+ filePath);
    }
  });

program.command('create')
  .alias('c')
  .description('创建api文档')
  .action((args: any) => {
    const path = program.getOptionValue('path') ?? './src/controller';
    const method = program.getOptionValue('method');
    const controller = program.getOptionValue('controller');
    const output = program.getOptionValue('output') ?? './apidoc';

    const fs = require('fs');
    const paths = require('path');
    const filePath = paths.resolve('./', './yapi.json');

    const config = {
      scanPath: path,
      methods: method?.split(','),
      controllers: controller?.split(','),
      output: output,
    } as config;
    if (fs.existsSync(filePath)) {
      const configJson = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
      console.log("配置文件内容,"+ JSON.stringify(configJson));
      importApi(config, true, configJson);
    }
    buildApiFile(config);
  })


program.parse(process.argv);

if (!process.argv.slice(2).length) {
  program.outputHelp();
}
