import {buildApiFile, config} from "./doc";
import {localFileConfigImportData} from "./yapi.import";

function importApi(config: config, isLocal: boolean, configJson: any) {
  //生成api文件
  const apidata = buildApiFile(config);

  if (isLocal) {
    localFileConfigImportData(apidata,configJson);
  }
}

export default importApi;
