/**
 * 首页2
 */
@Controller('/index1')
export class Index1Controller {

  /**
   * 接口3
   * @param id
   */
  @Post('/home3')
  async home3(@Body() dto: Dto): Promise<Response<Vo>> {
    return null;
  }

  /**
   * 直接指定参数4
   * @param id 唯一标识
   * @param age 年龄
   */
  @Post('/home4')
  async home4(@Body('id') id: number,@Body('age') age: number): Promise<Response<Vo>> {
    return null;
  }

  /**
   * 带分页的接口5
   * @param id
   */
  @Post('/list')
  async list5(@Body() dto: DtoPage): Promise<Response<PageVo<Vo>>> {
    return null;
  }

  /**
   * 文件上传6
   * @param files
   * @param dto
   */
  @Post('/files')
  async files6(@Files() files, @Fields() dto:Dto): Promise<Response<PageVo<Vo>>> {
    return null;
  }

  /**
   * 文件上传7.
   * @file
   * @param files
   * @param dto
   */
  @Post('/download')
  async download7(@Body() dto: Dto): Promise<Response<PageVo<Vo>>> {
    return null;
  }

  /**
   * 测试8
   * @param files
   * @param dto
   */
  @Post('/test8')
  async test8(@Body() dto: Dto): Promise<Response<any>> {
    return null;
  }
}

//创建controller , post body 装饰器
function Controller(s: string) {
  return function (target: any) {
    console.log(target);
  }
}

function Post(s: string) {
  return function (target: any, propertyKey: string) {
    console.log(target);
    console.log(propertyKey);
  }
}

function Body(name?: string) {
  return function (target: any, propertyKey: string, parameterIndex: number) {
    console.log(target);
    console.log(propertyKey);
    console.log(parameterIndex);
  }
}

function Files(files?:string) {
    return function (target, propertyKey, parameterIndex) {
        console.log(target);
        console.log(propertyKey);
        console.log(parameterIndex);
    }
}

function Fields(files?:string) {
  return function (target, propertyKey, parameterIndex) {
    console.log(target);
    console.log(propertyKey);
    console.log(parameterIndex);
  }
}

const RuleType = {
  number: ()=> {

    return {
      required() {
        return RuleType;
      }
    }
  }
}

function Rule(type?:typeof RuleType) {
  return function (target, propertyKey) {
    console.log(target);
    console.log(propertyKey);
  }
}

type Response<T> = {
  //说明
  msg: string;
  //状态码
  code: number;
  //内容
  content: T;
}



enum STATUS {
  //学习
  STUDY = 1,
  //工作
  WORK = 2,
}

class Dto {
  //唯一标识
  @Rule(RuleType.number().required())
  id: number;
  //名称
  @Rule(RuleType.number().required())
  name: string;
  //内部对象
  internal: InternalDto;
}

class Level {
  //等级
  level: number;
  //名称数组
  name: string[];
}

class InternalDto {
  //用户id
  userId: number;
  //用户名称
  @Rule(RuleType.number().required())
  userName: string;
  //父级名称
  parentName: string;
  //忽略这个字段 @ignored
  id: number;
  //等级数组
  level: Level[];
}

class PageDto  {
  //页码
  page: number;
  //每页数
  limit: number;
}

class DtoPage extends PageDto{
  //唯一标识
  id: number;
  //名称
  name: string;
}

class Vo {
  // 年龄2
  age: number;
  //颜色
  color: string;
  //状态
  status: STATUS;
  //子节点
  children: Vo;
}

class PageVo<T> {
  //总数
  total: number;
  //列表
  list: T[];
  //页数
  page: number;
  //每页数
  limit: number;
}
