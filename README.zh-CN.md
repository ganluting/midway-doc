# midway-doc
 此插件是针对midway通过注释生成openapi标准的json文件，用户接口文档的自动生成。
## 快速开始

### 说明
适用于midwayjs 其他框架需做调整, 安装方法:
```shell
  npm install -g  .
```
执行命令:
```shell
  doc create
```
默认解析./src/controller目录下所有文件, 生成openapi.json文件到./docapi目录下。

### 解析规则

1. 字段注释标记 @ignored 该字段会被忽略
2. @Rule(RuleType.number().required()) 该字段为必填项
3. controller 参数与返回值务必能被正确推导，否则请直接指定类型
```ts
   // 强制指定返回类型
   function home(@Body() dto: Dto): Promise<Response<Vo>> {}
```
controller 文件格式如下:
```ts
/**
 * 首页
 */
@Controller('/index')
export class IndexController {

  /**
   * 接口
   * @param id
   */
  @Post('/home')
  async home(@Body() dto: Dto): Promise<Response<Vo>> {
    return null;
  }

  /**
   * 带分页的接口
   * @param id
   */
  @Post('/list')
  async list(@Body() dto: DtoPage): Promise<Response<PageVo<Vo>>> {
    return null;
  /**
   * 直接指定参数
   * @param id 唯一标识
   * @param age 年龄
   */
  @Post('/home1')
  async home1(@Body('id') id: number,@Body('age') age: number): Promise<Response<Vo>> {
    return null;
  }
}


enum STATUS {
  //学习
  STUDY = 1,
  //工作
  WORK = 2,
}

class Dto {

  /**
   * 带分页的接口
   * @param id
   */
  @Post('/list')
  async list(@Body() dto: DtoPage): Promise<Response<PageVo<Vo>>> {
    return null;
  }

  /**
   * 文件上传
   * @param files
   * @param dto
   */
  @Post('/files')
  async files(@Files() files, @Fields() dto:Dto): Promise<Response<PageVo<Vo>>> {
    return null;
  }

  /**
   * 文件上传
   * @file
   * @param files
   * @param dto
   */
  @Post('/download')
  async download(@Body() dto: Dto): Promise<Response<PageVo<Vo>>> {
    return null;
  }
}

//创建controller , post body 装饰器
function Controller(s: string) {
  return function (target: any) {
    console.log(target);
  }
}

function Post(s: string) {
  return function (target: any, propertyKey: string) {
    console.log(target);
    console.log(propertyKey);
  }
}

function Body(name?: string) {
  return function (target: any, propertyKey: string, parameterIndex: number) {
    console.log(target);
    console.log(propertyKey);
    console.log(parameterIndex);
  }
}

function Files(files?:string) {
  return function (target, propertyKey, parameterIndex) {
    console.log(target);
    console.log(propertyKey);
    console.log(parameterIndex);
  }
}

function Fields(files?:string) {
  return function (target, propertyKey, parameterIndex) {
    console.log(target);
    console.log(propertyKey);
    console.log(parameterIndex);
  }
}

type Response<T> = {
  //说明
  msg: string;
  //状态码
  code: number;
  //内容
  content: T;
}



enum STATUS {
  //学习
  STUDY = 1,
  //工作
  WORK = 2,
}

class Dto {
  //唯一标识
  id: number;
  //名称
  name: string;
}

class PageDto  {
  //页码
  page: number;
  //每页数
  limit: number;
}

class DtoPage extends PageDto{
  //唯一标识
  id: number;
  //名称
  name: string;
}

class PageDto  {
  //页码
  page: number;
  //每页数
  limit: number;
}

class DtoPage extends PageDto{
  //唯一标识
  id: number;
  //名称
  name: string;
}

class Vo {
  // 年龄
  age: number;
  //颜色
  color: string;
  //状态
  status: STATUS;
  //子节点
  children: Vo;
}

class PageVo<T> {
  //总数
  total: number;
  //列表
  list: T[];
  //页数
  page: number;
  //每页数
  limit: number;
}


class Vo {
  // 年龄
  age: number;
  //颜色
  color: string;
  //状态
  status: STATUS;
  //子节点
  children: Vo;
}

class PageVo<T> {
  //总数
  total: number;
  //列表
  list: T[];
  //页数
  page: number;
  //每页数
  limit: number;
}

```
### 其他命令
```shell
> doc create -m home //只生成home方法
> doc help
Options:
  -V, --version                  output the version number
  -p ,--path <path>              解析一个目录,默认为当前目录下./src/controller
  -m ,--method <method>          解析多个method逗号分隔
  -c ,--controller <controller>  解析多个controller逗号分隔
  -o ,--output <output>          输出目录
  -h, --help                     display help for command

Commands:
  create|c                       创建api文档
  help [command]                 display help for command
```

